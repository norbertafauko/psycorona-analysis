# Run all R script within `src` directory

fname <- list.files("src", pattern = "*.R", all.files = TRUE, full.name = TRUE)

lapply(fname, function(f) {
  print(sprintf("Running script %s", f))
  source(f)
  print("Cleaning up the workspace")
  rm(list = ls())
})
